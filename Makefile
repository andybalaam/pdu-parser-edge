all: test

test:
	cargo fmt
	cargo test

run:
	cargo fmt
	wrangler dev

deploy:
	wrangler publish
