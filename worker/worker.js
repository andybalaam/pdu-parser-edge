addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

/**
 * @param {Request} request
 */
async function handleRequest(request) {
    const { parse } = wasm_bindgen;
    await wasm_bindgen(wasm);
    const response = parse(await request.text());
    const status = response.startsWith('{"error"') ? 400 : 200;
    return new Response(response, {status});
}
