# pdu-parser-edge

An [SMPP](https://smpp.org/) PDU parser that runs in the Cloudflare Edge
network.

## Use it without installing anything

Provide the hex-encoded bytes of a PDU and the response will be JSON describing
the contents of that PDU.

```bash
$ curl --data '0000002000000009000000000000000242004300440034000000' https://pdu-parser-edge.stonewareprimate.workers.dev
{"addr_npi":"00","addr_ton":"00","address_range":"","command_id":"00000009","command_length":"00000020","command_status":"00000000","interface_version":"34","password":"C","sequence_number":"00000002","system_id":"B","system_type":"D"}
```

## Build and run

* [Install Rust](https://www.rust-lang.org/tools/install).

* [Install Cloudflare
Wrangler](https://developers.cloudflare.com/workers/get-started/guide)

* Log in:

```bash
wrangler config
```

(Note: `wrangler login` didn't work for me.)

* Build and run:

```bash
make run
```

## Publishing releases

```bash
cargo update
vim CHANGELOG.md   # Set the version number
make deploy
git tag $VERSION
git push --tags
```

## Code of conduct

We follow the [Rust code of conduct](https://www.rust-lang.org/conduct.html).

Currently the moderation team consists of Andy Balaam only.  We would welcome
more members: if you would like to join the moderation team, please contact
Andy Balaam.

Andy Balaam may be contacted by email on andybalaam at artificialworlds.net or
on mastodon on
[@andybalaam@mastodon.social](https://mastodon.social/web/accounts/7995).

## License

pdu-parser-edge is distributed under the terms of both the [MIT
license](LICENSE-MIT) and the [Apache License (Version 2.0)](LICENSE-APACHE).

This project is developed in both my work and personal time, and released under
my personal copyright with the agreement of my employer.
