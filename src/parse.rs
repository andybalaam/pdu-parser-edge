use serde_json::json;
use serde_json::map::Map;
use smpp_pdu::pdu::formats::{COctetString, Integer1, Integer4};
use smpp_pdu::pdu::{
    BindReceiverPdu, BindTransceiverPdu, BindTransmitterPdu, EnquireLinkPdu,
    Pdu, PduBody, SubmitSmPdu,
};
use std::io::Cursor;

pub fn parse(hex_bytes: &str) -> serde_json::Value {
    match do_parse(hex_bytes) {
        Ok(s) => s,
        Err(e) => e,
    }
}

fn do_parse(hex_bytes: &str) -> Result<serde_json::Value, serde_json::Value> {
    let bytes = hex::decode(hex_bytes).map_err(err_json)?;
    let mut cursor = Cursor::new(&bytes);
    let pdu = Pdu::parse(&mut cursor).map_err(err_json)?;

    cursor.set_position(0);
    let command_length = Integer4::read(&mut cursor).map_err(err_json)?;

    Ok(as_json(pdu, command_length))
}

fn err_json<T: ToString>(e: T) -> serde_json::Value {
    json!({"error": e.to_string()})
}

fn as_json(pdu: Pdu, command_length: Integer4) -> serde_json::Value {
    let mut ret = match pdu.body() {
        PduBody::BindTransceiver(body) => bind_transceiver_as_json(body),
        PduBody::BindReceiver(body) => bind_receiver_as_json(body),
        PduBody::BindTransmitter(body) => bind_transmitter_as_json(body),
        PduBody::EnquireLink(body) => enquire_link_as_json(body),
        PduBody::SubmitSm(body) => submit_sm_as_json(body),
        // TODO: more types
        // TODO: extra properties that explain the types e.g. command_type, short message
        _ => Map::new(),
    };

    insert_integer4(&mut ret, "command_length", &command_length);
    insert_integer4(&mut ret, "command_id", &pdu.command_id());
    insert_integer4(&mut ret, "command_status", &pdu.command_status);
    insert_integer4(&mut ret, "sequence_number", &pdu.sequence_number);

    serde_json::Value::Object(ret)
}

fn insert_integer4(
    map: &mut Map<String, serde_json::Value>,
    name: &str,
    input: &Integer4,
) {
    map.insert(
        String::from(name),
        serde_json::Value::String(hex::encode(input.value.to_be_bytes())),
    );
}

fn insert_integer1(
    map: &mut Map<String, serde_json::Value>,
    name: &str,
    input: &Integer1,
) {
    map.insert(
        String::from(name),
        serde_json::Value::String(hex::encode([input.value])),
    );
}

fn insert_coctetstring(
    map: &mut Map<String, serde_json::Value>,
    name: &str,
    input: &COctetString,
) {
    map.insert(
        String::from(name),
        serde_json::Value::String(input.value.to_string()),
    );
}

fn enquire_link_as_json(
    _body: &EnquireLinkPdu,
) -> Map<String, serde_json::Value> {
    Map::new()
}

fn bind_receiver_as_json(
    body: &BindReceiverPdu,
) -> Map<String, serde_json::Value> {
    let mut ret = Map::new();
    insert_coctetstring(&mut ret, "system_id", &body.0.system_id);
    insert_coctetstring(&mut ret, "password", &body.0.password);
    insert_coctetstring(&mut ret, "system_type", &body.0.system_type);
    insert_integer1(&mut ret, "interface_version", &body.0.interface_version);
    insert_integer1(&mut ret, "addr_ton", &body.0.addr_ton);
    insert_integer1(&mut ret, "addr_npi", &body.0.addr_npi);
    insert_coctetstring(&mut ret, "address_range", &body.0.address_range);
    ret
}

fn bind_transceiver_as_json(
    body: &BindTransceiverPdu,
) -> Map<String, serde_json::Value> {
    let mut ret = Map::new();
    insert_coctetstring(&mut ret, "system_id", &body.0.system_id);
    insert_coctetstring(&mut ret, "password", &body.0.password);
    insert_coctetstring(&mut ret, "system_type", &body.0.system_type);
    insert_integer1(&mut ret, "interface_version", &body.0.interface_version);
    insert_integer1(&mut ret, "addr_ton", &body.0.addr_ton);
    insert_integer1(&mut ret, "addr_npi", &body.0.addr_npi);
    insert_coctetstring(&mut ret, "address_range", &body.0.address_range);
    ret
}

fn bind_transmitter_as_json(
    body: &BindTransmitterPdu,
) -> Map<String, serde_json::Value> {
    let mut ret = Map::new();
    insert_coctetstring(&mut ret, "system_id", &body.0.system_id);
    insert_coctetstring(&mut ret, "password", &body.0.password);
    insert_coctetstring(&mut ret, "system_type", &body.0.system_type);
    insert_integer1(&mut ret, "interface_version", &body.0.interface_version);
    insert_integer1(&mut ret, "addr_ton", &body.0.addr_ton);
    insert_integer1(&mut ret, "addr_npi", &body.0.addr_npi);
    insert_coctetstring(&mut ret, "address_range", &body.0.address_range);
    ret
}

fn submit_sm_as_json(body: &SubmitSmPdu) -> Map<String, serde_json::Value> {
    let mut ret = Map::new();
    insert_coctetstring(&mut ret, "service_type", &body.0.service_type);
    insert_integer1(&mut ret, "source_addr_ton", &body.0.source_addr_ton);
    insert_integer1(&mut ret, "source_addr_npi", &body.0.source_addr_npi);
    insert_coctetstring(&mut ret, "source_addr", &body.0.source_addr);
    insert_integer1(&mut ret, "dest_addr_ton", &body.0.dest_addr_ton);
    insert_integer1(&mut ret, "dest_addr_npi", &body.0.dest_addr_npi);
    insert_coctetstring(&mut ret, "destination_addr", &body.0.destination_addr);
    insert_integer1(&mut ret, "esm_class", &body.0.esm_class);
    insert_integer1(&mut ret, "protocol_id", &body.0.protocol_id);
    insert_integer1(&mut ret, "priority_flag", &body.0.priority_flag);
    insert_coctetstring(
        &mut ret,
        "schedule_delivery_time",
        &body.0.schedule_delivery_time,
    );
    insert_coctetstring(&mut ret, "validity_period", &body.0.validity_period);
    insert_integer1(
        &mut ret,
        "registered_delivery",
        &body.0.registered_delivery,
    );
    insert_integer1(
        &mut ret,
        "replace_if_present_flag",
        &body.0.replace_if_present_flag,
    );
    insert_integer1(&mut ret, "data_coding", &body.0.data_coding);
    insert_integer1(&mut ret, "sm_default_msg_id", &body.0.sm_default_msg_id);
    ret.insert(
        String::from("short_message"),
        serde_json::Value::String(String::from("TODO")),
    );
    ret.insert(
        String::from("tlvs"),
        serde_json::Value::String(String::from("TODO")),
    );
    ret
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn format_enquire_link() {
        assert_eq!(
            parse("00000010000000150000000000000001"),
            json!({
                "command_id": "00000015",
                "command_length": "00000010",
                "command_status": "00000000",
                "sequence_number": "00000001"
            })
        );
    }

    #[test]
    fn format_bind_transceiver() {
        assert_eq!(
            parse("0000002000000009000000000000000242004300440034000000"),
            json!({
                "command_id": "00000009",
                "command_length": "00000020",
                "command_status": "00000000",
                "sequence_number": "00000002",
                "addr_npi": "00",
                "addr_ton": "00",
                "address_range": "",
                "interface_version": "34",
                "password": "C",
                "system_id": "B",
                "system_type": "D"
            })
        );
    }

    #[test]
    fn format_submit_sm() {
        assert_eq!(
            parse(
                "0000003d000000040000000000000003000000450000004600\
                0001010000010003000447484748"
            ),
            json!({
                "command_id": "00000004",
                "command_length": "0000003d",
                "command_status": "00000000",
                "data_coding": "03",
                "dest_addr_npi": "00",
                "dest_addr_ton": "00",
                "destination_addr": "F",
                "esm_class": "00",
                "priority_flag": "01",
                "protocol_id": "01",
                "registered_delivery": "01",
                "replace_if_present_flag": "00",
                "schedule_delivery_time": "",
                "sequence_number": "00000003",
                "service_type": "",
                "short_message": "TODO",
                "sm_default_msg_id": "00",
                "source_addr": "E",
                "source_addr_npi": "00",
                "source_addr_ton": "00",
                "tlvs": "TODO",
                "validity_period": ""
            })
        );
    }

    #[test]
    fn format_non_hex() {
        assert_eq!(
            parse("G0000010000000150000000000000001"),
            json!({
                "error": "Invalid character \'G\' at position 0"
            })
        );
    }

    #[test]
    fn format_invalid_pdu() {
        assert_eq!(
            parse("000000100000d0150000000000000001"),
            json!({
                "error": "Error parsing PDU (\
                        command_id=0x0000D015, \
                        command_status=0x00000000, \
                        sequence_number=0x00000001, \
                        field_name=UNKNOWN\
                    ): Supplied command_id is unknown."
            })
        );
    }
}
